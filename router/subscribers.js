const express = require('express');
const router = express.Router()
const Subscriber = require('../models/subscriber')

// get all
router.get('/', (req, res) => 
{
        const subscribers =  Subscriber.find({}, (err, docs) =>
        {
            res.json(docs)
        })
})

// get one
router.get('/:id', (req, res) => 
{
    res.send("Get Param  " + req.params.id)
})

// create one
router.post('/', async (req, res) =>
{
    // const subscriber = new Subscriber
    // ({
    //     name: req.body.name,
    //     body: req.body.body
    // })
    // try
    // {
    //     const newSubscriber = await subscriber.save()
    //     res.status(201).json(newSubscriber)
    // }
    // catch(err)
    // {
    //     res.status(400).json({message: err.message})
    // }
})

// update one
router.patch('/:id', (req, res) =>
{
    
})

// delete one
router.delete('/:id', (req, res) =>
{
    
})

module.exports = router