const express = require('express');
const app = express();
const mongoose = require('mongoose');

//connect to the database
mongoose.connect('mongodb://localhost:27017/subscribers', {useNewUrlParser:true});
const db = mongoose.connection
db.on('error', (error) => console.error(error))
db.once('open', () => console.log('Connected to Database.'))

app.use(express.json())

// Routing to router folder
const subscribersRouter = require('./router/subscribers.js')
app.use('/subscribers', subscribersRouter)

// /*********Requesting to url '/'*********/
app.get('/name/:name', (req, res)=>
{
    res.send(req.params.name+" has joined the channel...")
})

//Listening to ports
const port = process.env.PORT || 3000
app.listen(port,()=> console.log(`Listening on port ${port}`) )